import sys

num_squared_lc = [i ** 2 for i in range(100000)]

num_squared_ge = (i ** 2 for i in range(100000))

print(f"num_squared_lc in RAM: {sys.getsizeof(num_squared_lc)}")

print(f"num_squared_ge in RAM: {sys.getsizeof(num_squared_ge)}")