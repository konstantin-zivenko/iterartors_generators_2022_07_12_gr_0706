from datetime import date, timedelta


def get_data(
        start: date,
        stop: date,
        step: int = 1
):
    current_date = start
    while current_date < stop:
        yield current_date
        current_date += timedelta(days=step)


for day in get_data(date(2022, 7, 1), date(2022, 7, 10), step=3):
    print(day)