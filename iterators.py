# Ітератори

# iterator = iter(obj) --> obj.__iter__()

# next(iterator) --> iterator.__next__()

from datetime import date, timedelta
# range(start, stop, step)

class DataRange:
    def __init__(
            self,
            start: date,
            stop: date,
            step: int,
    ):
        self.start = start
        self.stop = stop
        self.step = step

    def __iter__(self):
        self.current = self.start - timedelta(days=1)
        return self

    def __next__(self):
        self.current += timedelta(days=1)
        if self.current >= self.stop:
            raise StopIteration
        return self.current

for day in DataRange(date(2022, 7, 1), date(2022, 7, 10), step=1):
    print(day)

